# ROS 2 пакеты для управления двигателем

## Пререквизиты

- ROS 2 Humble

## Сборка и запуск ROS2 пакета

```bash
colcon build --symlink-install
```

```bash
source install/setup.bash
```

```bash
ros2 launch rbs_servo_test startup.launch.py
```

Посмотреть состояние топиков:
```bash
ros2 topic echo /joint_states
```
