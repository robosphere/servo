#!/usr/bin/env python3

import rclpy
from rclpy.node import Node
from std_msgs.msg import Float64MultiArray, MultiArrayLayout, MultiArrayDimension


class Float64Publisher(Node):

    def __init__(self):
        super().__init__('float64_publisher')
        self.publisher_ = self.create_publisher(Float64MultiArray, '/forward_position_controller/commands', 10)
        # timer_period = 60000  # seconds
        # self.timer = self.create_timer(timer_period, self.publish_float64_array)
        self.get_logger().info('Float64Publisher node started.')

    def publish_float64_array(self):
        msg = Float64MultiArray()

        # Specify layout
        dim = MultiArrayDimension()
        dim.label = 'example_dim'
        dim.size = 10
        dim.stride = 10

        layout = MultiArrayLayout()
        layout.dim.append(dim)
        layout.data_offset = 0

        msg.layout = layout

        # Specify data
        msg.data = [50.0]  # Example float64 data

        self.publisher_.publish(msg)
        self.get_logger().info('Publishing: "%s"' % msg.data)


def main(args=None):
    rclpy.init(args=args)
    publisher = Float64Publisher()
    publisher.publish_float64_array();
    rclpy.spin_once(publisher)
    publisher.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()