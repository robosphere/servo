#include <chrono>
#include <memory>
#include <string>
#include <rclcpp/rclcpp.hpp>
#include <std_msgs/msg/float32.hpp>
#include <linux/can.h>
#include <linux/can/raw.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <unistd.h>

using std::placeholders::_1;

class BLDCMotorControlNode : public rclcpp::Node
{
public:
    BLDCMotorControlNode() : Node("bldc_motor_control_node")
    {
        this->declare_parameter<std::string>("can_interface", "can0");

        this->get_parameter("can_interface", can_interface_);

        if ((can_socket_ = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
            RCLCPP_ERROR(this->get_logger(), "Failed to create CAN socket");
            throw std::runtime_error("Failed to create CAN socket");
        }

        struct ifreq ifr;
        strncpy(ifr.ifr_name, can_interface_.c_str(), IFNAMSIZ - 1);
        ifr.ifr_name[IFNAMSIZ - 1] = '\0';
        if (ioctl(can_socket_, SIOCGIFINDEX, &ifr) < 0) {
            RCLCPP_ERROR(this->get_logger(), "Failed to get interface index");
            throw std::runtime_error("Failed to get interface index");
        }

        struct sockaddr_can addr;
        addr.can_family = AF_CAN;
        addr.can_ifindex = ifr.ifr_ifindex;
        if (bind(can_socket_, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
            RCLCPP_ERROR(this->get_logger(), "Failed to bind CAN socket");
            throw std::runtime_error("Failed to bind CAN socket");
        }

        subscription_ = this->create_subscription<std_msgs::msg::Float32>(
            "motor_angle", 10, std::bind(&BLDCMotorControlNode::listener_callback, this, _1));
    }

    ~BLDCMotorControlNode()
    {
        close(can_socket_);
    }

private:
    void listener_callback(const std_msgs::msg::Float32::SharedPtr msg)
    {
        float angle = msg->data;
        send_motor_command(angle);
    }

    void send_motor_command(float angle)
    {
        struct can_frame frame;
        frame.can_id = 0x2; // Replace with your desired CAN ID
        std::stringstream ss;
        // Serialize speed to string with 4 decimal places
        ss << std::fixed << std::setprecision(3);
        ss << angle;
        std::string angle_string = ss.str();
        frame.can_dlc = angle_string.size(); // Data length code
        std::copy(angle_string.begin(), angle_string.end(), frame.data);
        // frame.data[speed_string.size() + 1] = '\0'
; // You'll need to serialize speed to fit CAN frame

        if (write(can_socket_, &frame, sizeof(frame)) != sizeof(frame)) {
            RCLCPP_ERROR(this->get_logger(), "Failed to send CAN frame");
        } else {
            RCLCPP_INFO(this->get_logger(), "Sent CAN frame: angle=%f rad", angle);
        }
    }

    std::string can_interface_;
    int can_socket_;
    rclcpp::Subscription<std_msgs::msg::Float32>::SharedPtr subscription_;
};

int main(int argc, char *argv[])
{
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<BLDCMotorControlNode>());
    rclcpp::shutdown();
    return 0;
}