#include "rbs_servo_hardware/rbs_servo_actuator.hpp"
// Libs for fake connection
// WARN: Delete it in the future
#include <netdb.h>
// System libs
#include <chrono>
#include <cmath>
#include <limits>
#include <memory>
#include <rclcpp_lifecycle/state.hpp>
#include <vector>
// ROS specific libs
#include "hardware_interface/actuator_interface.hpp"
#include "hardware_interface/types/hardware_interface_return_values.hpp"
#include "hardware_interface/types/hardware_interface_type_values.hpp"
#include "rclcpp/rclcpp.hpp"
#include <linux/can.h>
#include <linux/can/raw.h>
#include <net/if.h>
#include <rclcpp/logger.hpp>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <unistd.h>

namespace rbs_servo_hardware {
hardware_interface::CallbackReturn
RbsServoActuator::on_init(const hardware_interface::HardwareInfo &info) {
  if (hardware_interface::ActuatorInterface::on_init(info) !=
      hardware_interface::CallbackReturn::SUCCESS) {
    return hardware_interface::CallbackReturn::ERROR;
  }

  hw_joint_target_angle_ = std::numeric_limits<double>::quiet_NaN();

  can_interface_ = info_.hardware_parameters["can_interface"];
  can_node_id_ = stoi(info_.hardware_parameters["can_node_id"]);

  const hardware_interface::ComponentInfo &joint = info_.joints[0];
  // RbsServoActuator has exactly one command interface and one joint
  if (joint.command_interfaces.size() != 1) {
    RCLCPP_FATAL(logger_,
                 "Joint '%s' has %zu command interfaces found. 1 expected.",
                 joint.name.c_str(), joint.command_interfaces.size());
    return hardware_interface::CallbackReturn::ERROR;
  }

  if (joint.command_interfaces[0].name != hardware_interface::HW_IF_POSITION) {
    RCLCPP_FATAL(logger_,
                 "Joint '%s' have %s command interfaces found. '%s' expected.",
                 joint.name.c_str(), joint.command_interfaces[0].name.c_str(),
                 hardware_interface::HW_IF_POSITION);
    return hardware_interface::CallbackReturn::ERROR;
  } else if (joint.state_interfaces[0].name !=
             hardware_interface::HW_IF_POSITION) {
    RCLCPP_FATAL(logger_,
                 "Joint '%s' have %s state interface found. '%s' expected.",
                 joint.name.c_str(), joint.state_interfaces[0].name.c_str(),
                 hardware_interface::HW_IF_POSITION);
    return hardware_interface::CallbackReturn::ERROR;
  }

  if ((can_socket_ = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
    RCLCPP_ERROR(logger_, "Failed to create CAN socket");
    return hardware_interface::CallbackReturn::ERROR;
  }

  struct ifreq ifr;
  strncpy(ifr.ifr_name, can_interface_.c_str(), IFNAMSIZ - 1);
  ifr.ifr_name[IFNAMSIZ - 1] = '\0';
  if (ioctl(can_socket_, SIOCGIFINDEX, &ifr) < 0) {
    RCLCPP_ERROR(logger_, "Failed to get interface index");
    return hardware_interface::CallbackReturn::ERROR;
  }

  struct sockaddr_can addr;
  addr.can_family = AF_CAN;
  addr.can_ifindex = ifr.ifr_ifindex;
  if (bind(can_socket_, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
    RCLCPP_ERROR(logger_, "Failed to bind CAN socket");
    return hardware_interface::CallbackReturn::ERROR;
  }

  // auto server = gethostbyname("localhost");

  // address_.sin_family = AF_INET;
  // bcopy(reinterpret_cast<char *>(server->h_addr),
  //       reinterpret_cast<char *>(&address_.sin_addr.s_addr),
  //       server->h_length);
  // address_.sin_port = htons(socket_port_);

  // RCLCPP_INFO(logger_,
  //             "Trying to connect to port %d.", socket_port_);
  // if (connect(sock_, (struct sockaddr *)&address_, sizeof(address_)) < 0) {
  //   RCLCPP_FATAL(logger_,
  //                "Connection over socket failed.");
  //   return hardware_interface::CallbackReturn::ERROR;
  // }
  RCLCPP_INFO(logger_, "Connected to socket");

  return hardware_interface::CallbackReturn::SUCCESS;
}

hardware_interface::CallbackReturn
on_configure(const rclcpp_lifecycle::State & /*previous_state*/) {

  return hardware_interface::CallbackReturn::SUCCESS;
}

hardware_interface::CallbackReturn RbsServoActuator::on_shutdown(
    const rclcpp_lifecycle::State & /*previous_state*/) {
  // shutdown(sock_, SHUT_RDWR); // shutdown socket

  return hardware_interface::CallbackReturn::SUCCESS;
}

std::vector<hardware_interface::StateInterface>
RbsServoActuator::export_state_interfaces() {
  std::vector<hardware_interface::StateInterface> state_interfaces;

  state_interfaces.emplace_back(hardware_interface::StateInterface(
      info_.joints[0].name, hardware_interface::HW_IF_POSITION,
      &hw_joint_angle_));

  return state_interfaces;
}

std::vector<hardware_interface::CommandInterface>
RbsServoActuator::export_command_interfaces() {
  std::vector<hardware_interface::CommandInterface> command_interfaces;

  command_interfaces.emplace_back(hardware_interface::CommandInterface(
      info_.joints[0].name, hardware_interface::HW_IF_POSITION,
      &hw_joint_target_angle_));

  return command_interfaces;
}

hardware_interface::CallbackReturn RbsServoActuator::on_activate(
    const rclcpp_lifecycle::State & /*previous_state*/) {
  // START: This part here is for exemplary purposes - Please do not copy to
  // your production code
  RCLCPP_INFO(logger_, "Activating ...please wait...");

  // set some default values for joints
  if (std::isnan(hw_joint_target_angle_)) {
    hw_joint_target_angle_ = 0;
    hw_joint_angle_ = 0;
  }

  // Создание CAN-кадра для включения двигателя
  struct can_frame frame;
  frame.can_id = can_node_id_;  // Идентификатор сообщения (замените на ваш ID)
  frame.can_dlc = 2;    // Длина данных (1 байт для флага + 1 байт для значения)
  frame.data[0] = 'E';  // Флаг для команды управления двигателем
  frame.data[1] = true; // Значение для включения двигателя (true -> 0x01)

  // Отправка сообщения
  if (::write(can_socket_, &frame, sizeof(frame)) != sizeof(frame)) {
    RCLCPP_ERROR(rclcpp::get_logger("RbsServoActuator"),
                 "Failed to send CAN frame for motor activation");
    return hardware_interface::CallbackReturn::ERROR;
  }

  RCLCPP_INFO(rclcpp::get_logger("RbsServoActuator"),
              "Motor activation command sent successfully");

  RCLCPP_INFO(rclcpp::get_logger("RbsServoActuator"),
              "Successfully activated!");

  return hardware_interface::CallbackReturn::SUCCESS;
}

hardware_interface::CallbackReturn RbsServoActuator::on_deactivate(
    const rclcpp_lifecycle::State & /*previous_state*/) {
  // START: This part here is for exemplary purposes - Please do not copy to
  // your production code
  RCLCPP_INFO(logger_, "Deactivating ...please wait...");

  // for (int i = 0; i < hw_stop_sec_; i++) {
  //   rclcpp::sleep_for(std::chrono::seconds(1));
  //   RCLCPP_INFO(logger_, "%.1f seconds
  //   left...",
  //               hw_stop_sec_ - i);
  // }

  RCLCPP_INFO(logger_, "Successfully deactivated!");
  // END: This part here is for exemplary purposes - Please do not copy to your
  // production code
  close(can_socket_);
  return hardware_interface::CallbackReturn::SUCCESS;
}

hardware_interface::return_type
RbsServoActuator::read(const rclcpp::Time & /*time*/,
                       const rclcpp::Duration & /*period*/) {
  struct can_frame frame;

  // Чтение кадров в цикле
  while (::read(can_socket_, &frame, sizeof(frame)) == sizeof(frame)) {
    // Проверка флага 'A'
    if (frame.data[0] == 'A') {
      // Извлекаем угол
      float angle;
      std::memcpy(&angle, &frame.data[1], sizeof(float));

      // Логируем полученный угол
      RCLCPP_DEBUG(logger_, "Received CAN frame with angle: %f rad", angle);

      // Обновляем состояние сустава
      hw_joint_angle_ = static_cast<double>(angle);
      return hardware_interface::return_type::OK;
    } else {
      // Игнорирование кадров с другими флагами
      RCLCPP_DEBUG(logger_, "Ignoring CAN frame with unknown flag: %c",
                   frame.data[0]);
    }
  }

  if (errno != EAGAIN) {
    RCLCPP_ERROR(logger_, "Failed to read CAN frame: %s", strerror(errno));
    return hardware_interface::return_type::ERROR;
  }

  return hardware_interface::return_type::OK;
}

hardware_interface::return_type rbs_servo_hardware::RbsServoActuator::write(
    const rclcpp::Time & /*time*/, const rclcpp::Duration & /*period*/) {

  struct can_frame frame;
  frame.can_id = can_node_id_;
  float angle = static_cast<float>(hw_joint_target_angle_);
  frame.data[0] = 'A';
  std::memcpy(&frame.data[1], &angle, sizeof(float));
  frame.can_dlc = 5;

  if (::write(can_socket_, &frame, sizeof(frame)) != sizeof(frame)) {
    RCLCPP_ERROR(logger_, "Failed to send CAN frame");
  } else {
    RCLCPP_DEBUG(logger_, "Sent CAN frame: angle=%f rad", angle);
  }

  return hardware_interface::return_type::OK;
}

} // namespace rbs_servo_hardware

#include "pluginlib/class_list_macros.hpp"

PLUGINLIB_EXPORT_CLASS(rbs_servo_hardware::RbsServoActuator,
                       hardware_interface::ActuatorInterface)
