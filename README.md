# Сервопривод Робосборщика

![servo printed images](img/robossembler-servo-reducer-exploding-view-01.jpg)
![servo printed images](img/robossembler-servo-reducer-exploding-view-02.jpg)

Сервопривод на базе бесщёточного двигателя постоянного тока, адаптированный для производства с помощью 3D-печати. Разработан для управления 6-осевым роботом-манипулятором [Robossembler Arm](https://gitlab.com/robossembler/roboarm-diy-version), но может использоваться и как самостоятельное изделие в составе других систем. Предусматривается две конструкции статора двигателя: один для изготовления с помощью 3D-печати, другой с помощью листовой электротехнеской стали, нарезаемой на лазерном станке.

Ключевые особенности:
- Высокая мощность (допустимость редуктора)
- Высокая скорость (компенсация наличия редуктора)
- Хорошая динамика (разгон-торможение)
- Возможность электрического тормоза

В состав репозитория включены модели редукторов двух типов для использования в составе сервопривода. Исходные файлы редукторов представлены в директории `src/REDUCTOR`. В настоящее время основным является прециссирующий редуктор с соотношением 1:43.

Для управления используется универсальная плата-контроллер, которая может быть использована в вариантах исполнения двигателей разных диаметров (на данный момент 50 мм и 70 мм) со сходными характеристиками обмоток. Контроллер управляется через CAN-интерфейс.

## Внешний вид

![](img/servo-reducer-assembled.jpg)

## Описание директорий

```[servo]
├── controller/                           # Плата контроллер
│   ├── fw/                               # Исходный код прошивки микроконтроллера
│   │   ├── embed/                        # Инструкция по сборке и загрузке прошивки
│   │   └── test/                         # Тесты для проверки встроенного ПО
│   └── hw/                               # Проект печатной платы контроллера в kicad
├── img/                                  # Изображения для README.md
├── motor/                                # Все файлы сборок и деталей моторов в формате Solidworks
├── reducer/                              # Проекты редукторов в формате Solidworks
├── ros2_environment/                     # Пакеты для управления мотором из ROS 2 с помощью ros2_control
└── tools/                                # Вспомогательное оборудование для тестирования, испытаний
    ├── conductor-for-fasteners-70mm/     # Проект оснастки для вкручивания винтов в пластиковый статор 72мм мотора
    ├── test-reductor-stend/              # Стенд для испытания редуктора
    └── torque-test-stend/                # Стенд для измерения усилия сервопривода
```

## Краткая инструкция по изготовлению

### Статор

Для удобства изготовления статоров разработан станок для намотки катушек индуктивности. Исходные файлы для производства станка и инструкции размещены в репозитории [gitlab.com/robossembler/cnc/motor-wire-winder](https://gitlab.com/robossembler/cnc/motor-wire-winder). 

### Сборка

1. Вставить 28 магнитов в ротор
2. Установить подшипник в статор
3. Установить проставку в статор между подшипником и платой
4. Припаять плату к обмоткам (схема обмоток приведена ниже) и установить ее в статор
5. Накрыть плату крышкой
6. Установить фиксирующий шплинт
7. Надеть на сборку статора ротор
8. Подключить разъем программирования XP3 и прошить с помощью ST-Link-совместимого программатора

## Фото прототипов

Первый прототип изготовленного печатного мотора диаметром 50мм.

![servo printed](img/first-prototype-rbs-servo-50mm.png)

Современная версия привода диаметром 70мм.

![](img/70mm-prototype-02-inside.jpg)

## Схемы намотки

| Двигатель 70мм | Двигатель 50мм |
| ----------- | ----------- |
| ![coil winder schema](img/coil_winder_schema.jpg) | ![coil winder schema](img/coil_winder_schema_50mm.jpg) |
