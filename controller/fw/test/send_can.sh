#!/bin/bash

# Read 8 byte ascii from $1 and send it to CAN bus

# <can_id>:
#  3 (SFF) or 8 (EFF) hex chars
# {data}:
#  0..8 (0..64 CAN FD) ASCII hex-values (optionally separated by '.')
# {len}:
#  an optional 0..8 value as RTR frames can contain a valid dlc field
# <flags>:
#  a single ASCII Hex value (0 .. F) which defines canfd_frame.flags

can_id=$1
input=$2

if [[ ${#input} -gt 8 ]]; then
  echo "Error: Input string must be no longer than 8 characters."
  exit 1
fi

hex_output=""
for (( i=0; i<${#input}; i++ )); do
  hex_char=$(printf "%02x" "'${input:$i:1}")
  hex_output+="${hex_char}"
done

hex_output=$(printf "%-16s" "$hex_output")
hex_output=${hex_output// /0}

can_id=$(printf "%03X" $can_id)

cansend can0 $can_id#$hex_output

