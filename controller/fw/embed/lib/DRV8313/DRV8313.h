#pragma once

#include "SimpleFOC.h"

class DRV8313Driver : public BLDCDriver3PWM {
 public:
    DRV8313Driver(int phA, int phB, int phC, int en1 = NOT_SET, int en2 = NOT_SET, int en3 = NOT_SET, int slp = NOT_SET,
                  int rst = NOT_SET, int flt = NOT_SET);

    int init() override;

    void enable() override;

    void disable() override;

 private:
    int slp_pin;
    int rst_pin;
    int flt_pin;
};
