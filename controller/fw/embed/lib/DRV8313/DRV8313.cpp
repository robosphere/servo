#include "DRV8313.h"

DRV8313Driver::DRV8313Driver(int phA, int phB, int phC, int en1, int en2, int en3, int slp, int rst,
                             int flt) : BLDCDriver3PWM(phA, phB, phC, en1, en2, en3), slp_pin(slp), rst_pin(rst),
                                        flt_pin(flt) {
}

int DRV8313Driver::init() {
    // Get state from flt pin
    if (_isset(flt_pin)) {
        pinMode(flt_pin, INPUT);
        if (digitalRead(flt_pin) == HIGH) {
            // if the fault pin is high the driver is in fault state
            // reset the driver
            if (_isset(rst_pin)) {
                pinMode(rst_pin, OUTPUT);
                digitalWrite(rst_pin, LOW);
                delay(1);
                digitalWrite(rst_pin, HIGH);
                delay(1);
            }
        }
    }
    return BLDCDriver3PWM::init();
}

void DRV8313Driver::enable() {
    // Enable the driver
    if (_isset(slp_pin)) {
        pinMode(slp_pin, OUTPUT);
        digitalWrite(slp_pin, HIGH);
    }
    BLDCDriver3PWM::enable();
}

void DRV8313Driver::disable() {
    if (_isset(slp_pin)) {
        pinMode(slp_pin, OUTPUT);
        digitalWrite(slp_pin, LOW);
    }
    BLDCDriver3PWM::disable();
}
