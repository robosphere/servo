#pragma once

#include "SimpleFOC.h"
#include "SPI.h"

#ifndef MSBFIRST
#define MSBFIRST BitOrder::MSBFIRST
#endif

#define AS5045_BITORDER MSBFIRST
#define AS5045_CPR 4096.0f
#define _2PI 6.28318530718f

static SPISettings AS5145SSISettings(1000000, AS5045_BITORDER, SPI_MODE0);

class MagneticSensorAS5045 final: public Sensor {
 public:
    MagneticSensorAS5045(uint16_t as5040_cs, uint16_t as5040_mosi, uint16_t as5040_miso, uint16_t as5040_sclk);

    virtual ~MagneticSensorAS5045();

    float getSensorAngle() override;

    virtual void init(SPIClass *_spi = &SPI);

    [[nodiscard]] uint16_t readRawAngleSSI() const;

 private:
    uint16_t AS5040_CS_, AS5040_MOSI_, AS5040_MISO_, AS5040_SCLK_;
    SPIClass *spi;
    SPISettings settings;
};
