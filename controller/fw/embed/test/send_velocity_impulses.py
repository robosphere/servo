import can
import struct
import time

# Function to send the target speed
def send_target_speed(bus, target_speed):
    msg = can.Message()
    msg.arbitration_id = 1  # Message ID
    msg.is_extended_id = False
    msg.dlc = 5  # Message length
    msg.data = [ord('V')] + list(struct.pack('<f', target_speed))  # 'V' for the command identifier, followed by the speed in float format

    try:
        bus.send(msg)
        print(f"Sent message with target speed: {target_speed} m/s")
        print(f"Message data: {msg.data}")
    except can.CanError:
        print("Message failed to send")

# Main function
def main():
    # CAN interface setup
    bus = can.interface.Bus(channel='can0', bustype='socketcan', bitrate=1000000)  # Ensure the bitrate matches the microcontroller settings
    print("CAN bus initialized, sending target speed impulses...")

    # Send impulses of target speed from -2 to 2 m/s
    target_speeds = [-1, 1]

    while True:
        for speed in target_speeds:
            send_target_speed(bus, speed)
            time.sleep(1)  # 1-second delay between messages

if __name__ == '__main__':
    main()
