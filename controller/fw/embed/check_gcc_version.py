Import("env")

# Получаем путь к компилятору из окружения PlatformIO
gcc_path = env.subst("$CC")

# Выполняем команду для получения версии компилятора
import subprocess

try:
    result = subprocess.run([gcc_path, "--version"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    if result.returncode == 0:
        print(f"GCC version: {result.stdout}")
    else:
        print(f"Failed to get GCC version: {result.stderr}")
except Exception as e:
    print(f"Error while getting GCC version: {e}")

# Дополнительно проверяем путь к компилятору
print(f"Compiler path: {gcc_path}")
