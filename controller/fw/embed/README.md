# Встроенное ПО для сервипривода на STM32F446RE

## Для разработки

- [Установить platformio](#introduction)
```bash
pip install -U platformio
```
- [Скомпилировать проект](#build_project)
```bash
platformio run --environment robotroller_reborn
```
- [Загрузить прошивку](#upload_project)
```bash
platformio run --target upload --environment robotroller_reborn 
```
- [Открыть монитор UART](#monitor_port)
```bash
platformio device monitor
```
